function getData() {
    $.ajax({
        url: "http://localhost:8090/api/categories"
    }).done(function (data) {
        var restObject = data;
        populateExpenses(restObject);
    });
}

function showAddDialog() {
    $("#addDialog").css("visibility", "visible");
    $("#addDialog").show(); //to może być zamiast powyższego pod warunkiem że nie ma w css "visibility: hidden;"
}

function hideAddDialog() {
    //$("#addDialog").css("visibility", "hidden"); 
    $("#addDialog").hide();
}

function removeExpense(categoriesIndex, expenseIndex) {

    $.ajax({
        type: "DELETE",
        url: "http://localhost:8090/api/categories/" + categoriesIndex + "/expenses/" + expenseIndex
    }).done(function (afterDelete) {
        clearTable();
        getData();
    }
);



}

function populateExpenses(categories) {
    let tbody = $("#table_body");
    let sum = 0;
    for (let categoriesIndex in categories) {

        let category = categories[categoriesIndex];
        let flag = true;
        for (let expenseIndex in category.expenses) {

            let expense = category.expenses[expenseIndex];
            sum += expense.cost;
            let tr = $("<tr></tr>");
            if (flag) { //dla pierwszego elementu w kategorii scal komórki
                flag = false;
                let catTD = $("<td></td>")
                        .attr("rowSpan", category.expenses.length)
                        .text(category.name);
                tr.append(catTD);
            }

            let nameTD = $("<td></td>").text(expense.name);
            let costTD = $("<td></td>").text(expense.cost);

            let removeTD = $("<td></td>");
            let removeButton = $("<button></button>").text("delete")
                    .on("click", function () {
                        removeExpense(categoriesIndex, expenseIndex);
                    });

            removeTD.append(removeButton);

            tr.append(nameTD, costTD, removeTD);

            tbody.append(tr);
        }
    }
    $("#sumCell").text(sum);
}

function add() {

    let catInput = $("#category").val();
    let nameInput = $("#name").val();
    let costInput = $("#cost").val();

    $.ajax({
        type: "POST",
        url: "http://localhost:8090/api/categories",
        data: JSON.stringify({name: catInput, expenses: [{name: nameInput, cost: costInput}]}),
        contentType: "application/json"

    }).done(
            function (doAfter) {
                clearTable();
                getData();
                hideAddDialog();
            }); //jak sie skończy request dodawanie - wyczyść tebele i załaduj dane do tabeli
}

function clearTable() {
    $("#table_body").empty();
    $("#sum").empty();
}
