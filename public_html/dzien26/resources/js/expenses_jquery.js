
var categories = [
    {
        name: "food",
        expenses: [
            {
                name: "pizza",
                cost: 20
            },
            {
                name: "burger",
                cost: 10
            }
        ]
    },
    {
        name: "toys",
        expenses: [{
                name: "Ninrendo Switch",
                cost: 1200
            },
            {
                name: "Zelda",
                cost: 250
            }
        ]
    }
];

function showAddDialog() {
    $("#addDialog").css("visibility", "visible");
    $("#addDialog").show(); //to może być zamiast powyższego pod warunkiem że nie ma w css "visibility: hidden;"
}

function hideAddDialog() {
    //$("#addDialog").css("visibility", "hidden"); 
    $("#addDialog").hide();
}

function removeExpense(expense) {
    for (let catIndex in categories) {
        let category = categories[catIndex];
        for (let exIndex in category.expenses) {
            if (category.expenses[exIndex] === expense) {
                category.expenses.splice(exIndex, 1);
                break;
            }
        }
    }
    clearTable();
    populateExpenses();
}

function populateExpenses() {
    let tbody = $("#table_body");
    let sum = 0;
    for (let categoriesIndex in categories) {

        let category = categories[categoriesIndex];
        let flag = true;
        for (let expenseIndex in category.expenses) {

            let expense = category.expenses[expenseIndex];
            sum += expense.cost;
            let tr = $("<tr></tr>");
            if (flag) { //dla pierwszego elementu w kategorii scal komórki
                flag = false;
                let catTD = $("<td></td>")
                        .attr("rowSpan", category.expenses.length)
                        .text(category.name);
                tr.append(catTD);
            }
            
            let nameTD=$("<td></td>").text(expense.name);
            let costTD=$("<td></td>").text(expense.cost);
            
            let removeTD = $("<td></td>");
            let removeButton = $("<button></button>").text("delete")
                    .on("click",function(){
                        removeExpense(expense);
            });
            
            removeTD.append(removeButton);
            
            tr.append(nameTD,costTD,removeTD);
            
            tbody.append(tr);
        }

    }
    $("#sumCell").text(sum);
}

function add() {

    let catInput = $("#category").val();
    let nameInput = $("#name").val();
    let costInput = $("#cost").val();

    let selectedCategory;//undefined
    for (let catIndex in categories) {
        let category = categories[catIndex];
        if (catInput === category.name) {
            selectedCategory = category;
            break;
        }
    }

    if (!selectedCategory) {
        selectedCategory = {name: catInput, expenses: []};
        categories.push(selectedCategory);
    }

    selectedCategory.expenses.push({name: nameInput, cost: parseFloat(costInput)});
    clearTable();
    populateExpenses();
    hideAddDialog();

}

function clearTable() {
    $("#table_body").empty();
    $("#sum").empty();

}
