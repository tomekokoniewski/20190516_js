
var categories = [
    {
        name: "food",
        expenses: [
            {
                name: "pizza",
                cost: 20
            },
            {
                name: "burger",
                cost: 10
            }
        ]
    },
    {
        name: "toys",
        expenses: [{
                name: "Ninrendo Switch",
                cost: 1200
            },
            {
                name: "Zelda",
                cost: 250
            }
        ]
    }
];
function showAddDialog(){
    let dialog = document.getElementById("addDialog");
    dialog.style.visibility = "visible";
}

function hideAddDialog(){
    let dialog = document.getElementById("addDialog");
    dialog.style.visibility = "hidden";

}

function removeExpense(expense){
    for(let catIndex in categories){
        let category = categories[catIndex];
        for(let exIndex in category.expenses){
            if(category.expenses[exIndex]===expense){
                category.expenses.splice(exIndex,1);
                break;
            }
        }
    }
    clearTable();
    populateExpenses();
}

function populateExpenses() {
    let tbody = document.getElementById("table_body");
    let sum = 0;
    for (let categoriesIndex in categories) {

        let category = categories[categoriesIndex];
        let flag = true;
        for (let expenseIndex in category.expenses) {

            let expense = category.expenses[expenseIndex];
            sum += expense.cost;
            let tr = document.createElement("tr");
            if (flag) { //dla pierwszego elementu w kategorii scal komórki
                flag = false;
                let catTD = document.createElement("td");
                let catText = document.createTextNode(category.name);
                tr.appendChild(catTD);
                catTD.appendChild(catText);
                catTD.rowSpan = category.expenses.length;
            }

            let nameTD = document.createElement("td");
            let costTD = document.createElement("td");
            let removeTD = document.createElement("td");
            
            let nameText = document.createTextNode(expense.name);
            let costText = document.createTextNode(expense.cost);
            let removeButton = document.createElement("button");
            removeButton.appendChild(document.createTextNode("delete"));
            removeButton.onclick = function(){
            //  alert(expense.name);  
            
            removeExpense(expense);
            
            
            };
            
            tr.appendChild(nameTD);
            tr.appendChild(costTD);
            tr.appendChild(removeTD);
            nameTD.appendChild(nameText);
            costTD.appendChild(costText);
            
            removeTD.appendChild(removeButton);
            
            tbody.appendChild(tr);
        }

    }
    let sumCell = document.getElementById("sum");
    sumCell.appendChild(document.createTextNode(sum));
}
function add() {

    let catInput = document.getElementById("category").value;
    let nameInput = document.getElementById("name").value;
    let costInput = document.getElementById("cost").value;
   
    let selectedCategory;//undefined
    for (let catIndex in categories) {
        let category = categories[catIndex];
        if (catInput === category.name) {
            selectedCategory = category;
            break;
        }
    }
    
      if (!selectedCategory) {
        selectedCategory = {name: catInput, expenses: []};
        categories.push(selectedCategory);
    }
    
    selectedCategory.expenses.push({name: nameInput, cost: parseFloat(costInput)});
    clearTable();
    populateExpenses();
    hideAddDialog();
    
}

function clearTable(){
    removeChildren(document.getElementById("table_body"));
    removeChildren(document.getElementById("sum"));
    
}



function removeChildren(node){
    while(node.firstChild){
        node.removeChild(node.firstChild);
    }
}