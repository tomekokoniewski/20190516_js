/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var goblins = [
    {
        name: "Puszek",
        height: 80,
        weapons: [
            {
                type: "rock",
                power: 20
            },
            {
                type: "second rock",
                power: 15
            }
        ]
    },
    {
        name: "Mruczek",
        height: 120,
        weapons: [
            {
                type: "stick",
                power: 5
            },
            {
                type: "fork",
                power: 10
            }
        ]
    }
];

/**
 * 
 * 
 * @param {type} a 
 * @param {type} b
 * @returns {undefined}
 */
function count(a, b) {
    if (a < b) {
        for (let i = a; i <= b; i++) {
            console.log(i);
        }
    } else {
        for (let i = b; i <= a; i++) {
            console.log(i);
        }
    }
}

/**
 * 
 * @param {type} a
 * @param {type} b
 * @param {type} step
 * @returns {unresolved}
 */
function process(a, b, step) {
    switch (step) {
        case 1:
            a *= -1;
        case 2:
            b **= 3;
        case 3:
            a += 2;
        case 4:
            a += b;
        case 5:
            b ^= a;
    }
    return a + b;
}

function printArray(array) {
    for (let index in array) {
        console.log(array[index]);
    }
}

function sortStringArray(array) {
    array.sort();
    printArray(array);
}

function sortIntArray(array) {
    array.sort(function (a, b) {
        return a - b;
    });
    printArray(array);
}

function sortStringSizeArray(array) {
    array.sort(function (a, b) {
        return a.length - b.length;
    });
    printArray(array);
}

function printNames() {
    for (let gobinIndex in goblins) {
        console.log(goblins[gobinIndex].name);
    }
}

function printNamesAndWeapons() {
    for (let goblinIndex in goblins) {
        console.log(goblins[goblinIndex].name);
        for (let weaponIndex in goblins[goblinIndex].weapons) {
            console.log(goblins[goblinIndex].weapons[weaponIndex].type);
        }
    }
}

function printNamesAndPowers() {
        for (let goblinIndex in goblins) {
        console.log(goblins[goblinIndex].name);
        let power = 0;
        for (let weaponIndex in goblins[goblinIndex].weapons) {
            power += goblins[goblinIndex].weapons[weaponIndex].power;
        }
        console.log(power);
    }
}